const random = require('random')
const { find } = require('geo-tz')
const { Sequelize, Model, DataTypes, Op } = require('sequelize')
const sequelize = new Sequelize('tz', 'urbantz', 'urbantz', {
  dialect: 'mysql',
  host: 'localhost'
});

class TimeZone extends Model {}

const geo2tz = (lat = null, lon = null) => {
  console.log('Finding timezone by geoloc')

  if (!lat) {
    lat = random.int(-90, 90)
  }

  if (!lon) {
    lon = random.int(-180, 180)
  }

  console.log(`lat: ${lat}\nlon: ${lon}`)
  const timezoneName = find(lat, lon)  // ['America/Los_Angeles']
  console.log(`timezone name: ${timezoneName}`)
}

const geo2tzhour = async (lat = null, lon = null) => {
  console.log('Finding timezone and hour diff by geoloc and tzdata')

  TimeZone.init({
    zone_name: DataTypes.STRING(35),
    country_code: DataTypes.CHAR(2),
    abbreviation:	DataTypes.STRING(6),
    time_start: DataTypes.DECIMAL(11,0),
    gmt_offset: DataTypes.INTEGER,
    dst: DataTypes.CHAR(1),
  }, { 
    sequelize, 
    tableName: 'time_zone',
    timestamps: false
  });

  TimeZone.removeAttribute('id')

  if (!lat) {
    lat = random.int(-90, 90)
  }

  if (!lon) {
    lon = random.int(-180, 180)
  }

  console.log(`lat: ${lat}\nlon: ${lon}`)
  const timezoneName = find(lat, lon)  // ['America/Los_Angeles']
  console.log(`timezone name: ${timezoneName}`)

  // Get the current possible timezone variation expected to work both with DST and non-DST
  const resultCurrent = await TimeZone.findOne({
    where: {
      zone_name: timezoneName,
      time_start: {
        [Op.lte]: Date.now() / 1000
      }
    },
    order: [['time_start', 'DESC']],
    limit: 1
  })

  const itemCurrent = resultCurrent ? resultCurrent.toJSON() : null
  if (itemCurrent) {
    itemCurrent.time_start_date = new Date(itemCurrent.time_start*1000)
  }

  console.log('itemCurrent:')
  console.log(itemCurrent)

  // Get the next possible timezone variation expected to work only with DST
  const resultNext = await TimeZone.findOne({
    where: {
      zone_name: timezoneName,
      time_start: {
        [Op.gte]: Date.now() / 1000
      }
    },
    order: [['time_start', 'ASC']],
    limit: 1
  })

  const itemNext = resultNext ? resultNext.toJSON() : null
  if (itemNext) {
    itemNext.time_start_date = new Date(itemNext.time_start*1000)
  }

  console.log('itemNext:')
  console.log(itemNext)

  // close db connection
  await sequelize.connectionManager.close()
}

// Brussels has DST changes twice a year
const brusselsTzHour = () => {
  return geo2tzhour(50, 4)
}

// Krasnoyarsk has DST changes every couple of years
const krasnoyarskTzHour = () => {
  return geo2tzhour(65, 105)
}

// Abu Dhabi has no DST
const abuDhabiTzHour = () => {
  return geo2tzhour(24, 54)
}

module.exports = {
  geo2tz,
  geo2tzhour,
  brusselsTzHour,
  krasnoyarskTzHour,
  abuDhabiTzHour
}