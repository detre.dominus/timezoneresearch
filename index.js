#!/usr/bin/env node
const yargs = require('yargs')

const f = require('./functions.js')

const argv = yargs(process.argv.splice(2))
  .command('geo2tz', 'runs a geolocation to timezone name conversion', () => {
    return yargs.options({
      'lat': {
        describe: 'latitude: from -90 to 90 degrees',
        number: true,
      },
      'lon': {
        describe: 'longitude: from -180 to 180 degrees',
        number: true,
      }
    }).help()
  }, (argv) => f.geo2tz(argv.lat, argv.lon))
  .command('geo2tzhour', 'runs a geolocation to timezone name and hour conversion', () => {
    return yargs.options({
      'lat': {
        describe: 'latitude: from -90 to 90 degrees',
        number: true,
      },
      'lon': {
        describe: 'longitude: from -180 to 180 degrees',
        number: true,
      }
    }).help()
  }, (argv) => f.geo2tzhour(argv.lat, argv.lon))
  .command('brusselsTzHour', 'runs a zone name and hour conversion for Brussels which has DST changes twice a year', () => {}, f.brusselsTzHour)
  .command('krasnoyarskTzHour', 'runs a zone name and hour conversion for Krasnoyarsk which has  DST switches every couple of years. last one was in 2014 and next one will be in 2038', () => {}, f.krasnoyarskTzHour)
  .command('abuDhabiTzHour', 'runs a zone name and hour conversion for Abu Dhabi which has no DST switches.', () => {}, f.abuDhabiTzHour)
  .demandCommand(1, 1, 'you need to choose a command')
  //.strict()
  .help('h')
  .argv